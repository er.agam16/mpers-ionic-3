import {HttpClient, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Api} from "..";
import {Platform} from "ionic-angular";
import {Storage} from "@ionic/storage";
import {map} from "rxjs/operators";

/*
  Generated class for the DeviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DeviceProvider {

  constructor(public api: Api, public plt: Platform, private storage: Storage) {
    console.log('Hello DevicePr');
  }

  getDevices() {
    return this.api.httpGet('devices');
  }

  getPositions() {
    return this.api.httpGet('positions');
  }

  getDeviceGeofences(deviceId) {
    return this.api.httpGet('geofences');
  }


  createNotification(notificationData) {
    return this.api.httpPostJson('notifications', JSON.stringify(notificationData));
  }

  sendCustomCommand(command, deviceId) {
    let commandData = {
      "attributes":
        {
          "data": command
        },
      "deviceId":deviceId,
      "type":"custom",
      "textChannel":false,
      "description":"Phone 1"
    };
    
    return this.api.httpPostJson('commands/send', JSON.stringify(commandData));
  }
  getAllNotifications(params = null) {
    if (params) {
      return this.api.httpGet('notifications' + params);
    } else {
      return this.api.httpGet('notifications');
    }

  }

}
