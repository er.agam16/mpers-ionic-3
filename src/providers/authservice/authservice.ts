import { Api } from '../api/api';
import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {Platform} from "ionic-angular";
import {map} from 'rxjs/operators';
import {HttpParams} from "@angular/common/http";
import { Storage } from '@ionic/storage';
import {UserActivationPage} from "../../pages/user-activation/user-activation";
const TOKEN_KEY = 'auth-user-data';

/*
  Generated class for the AuthserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class Authservice {
  authenticationState = new BehaviorSubject(false);

  constructor(public api: Api, public plt: Platform, private storage: Storage) {
    this.plt.ready().then(() => {
      this.checkToken();
    });
    console.log('Hello Authservice Authservice');
  }

  checkToken() {
    this.api.httpGet('session').subscribe(res => {
      if (res) {
        this.authenticationState.next(true);
      }
    });
  }

  login(activationCode) {
    const userData = new HttpParams()
      .set('email', activationCode + '@aditumhealth.com')
      .set('password', '12345');
    return this.api.httpPost('session', userData.toString()).pipe(map(user => {
      if (user) {
        // user = u();
        this.storage.set(TOKEN_KEY, JSON.stringify(user)).then(() => {
          this.authenticationState.next(true);
        });
      }
      return user;
    }));
  }


  loginAdmin() {
    const userData = new HttpParams()
      .set('email', 'admin')
      .set('password', 'Aditum@2018');
    return this.api.httpPost('session', userData.toString());
  }

  register(activationCode, data) {

    const userData = {
      'email': activationCode + '@aditumhealth.com',
      'password': '12345',
      'name': data['name'],
      'attributes': data,
      'deviceLimit' : -1,

    };
    return this.api.httpPostJson('users', JSON.stringify(userData)).pipe(map(user => {
      if (user) {
        // user = u();
        this.storage.set(TOKEN_KEY, JSON.stringify(user)).then(() => {
          this.authenticationState.next(true);
        });
      }
      return user;
    }));
  }

  addDeviceToUser(name, activationCode, devicePhone) {
    // const postData = {"name":name,"uniqueId":activationCode,"groupId":3};
    const postData = {"name":name,"uniqueId":activationCode, "phone": devicePhone};

    return this.api.httpPostJson('devices', JSON.stringify(postData));

  }


  verifyUserCode(activationCode) {
    return this.api.httpGetAditum('http://www.mocky.io/v2/5bbee45834000064006fcc27');
  }


  attachPermissions(userData) {
    return this.api.httpPostJson('permissions', JSON.stringify(userData));
  }

  removePermissions(userData) {
    return this.api.httpDeleteSpecial('permissions', JSON.stringify(userData));
  }


  deleteAllCookies() {
    return this.storage.remove(TOKEN_KEY).then(() => {
      this.authenticationState.next(false);
    });
  }

  logout() {
    return this.api.httpDelete('session').pipe(map(user => {
      return this.storage.remove(TOKEN_KEY).then(() => {
        this.authenticationState.next(false);
      });
    }, (err) => {
      return this.storage.remove(TOKEN_KEY).then(() => {
        this.authenticationState.next(false);
      });
    }));
  }

  checkUserSession() {
    return this.api.httpGet('session');
  }


  isLoggedIn() {
    return this.authenticationState.asObservable();
  }

}
