import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {
  url: string = 'http://169.53.186.146:8082/api/';

  constructor(private http: HttpClient) {
  }

  async httpGetAsAsync(url: string, options?: any) {
    const httpHeaders = new HttpHeaders({
      'Accept': '*/*'
    });

    if (options) {
      options.withCredentials = true;
      return await this.http.get('' + url, options);
    } else {
      return await this.http.get(this.url + url, {
        headers: httpHeaders,
        withCredentials: true
        // responseType: 'json'
      });
    }

  }


  httpGet(url: string, options?: any) {
    const httpHeaders = new HttpHeaders({
      'Accept': '*/*'
    });

    if (options) {
      options.withCredentials = true;
      return this.http.get('' + url, options);
    } else {
      return this.http.get(this.url + url, {
        headers: httpHeaders,
        withCredentials: true
        // responseType: 'json'
      });
    }

  }
  httpPost(url: string, param: any) {
    console.log('Calling Post Method');
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': '*/*'
    });
    return this.http.post(this.url + url, param, { headers: httpHeaders, withCredentials: true});
  }

  httpPostJson(url: string, param: any) {
    console.log('Calling Post Method');
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': '*/*'
    });
    return this.http.post(this.url + url, param, { headers: httpHeaders, withCredentials: true});
  }

  httpPutJson(url: string, param: any) {
    console.log('Calling Post Method');
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': '*/*'
    });
    
    return this.http.put(this.url + url, param, { headers: httpHeaders, withCredentials: true});
  }

  httpGetAditum(url: string) {
    console.log('Calling Aditum Get Method');
    return this.http.get(url);
  }

  httpDeleteSpecial(url: string, data: any) {
    console.log('Calling delete special Method');
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': '*/*'
    });
    return this.http.request('delete', this.url + url, { headers: httpHeaders, body: data, withCredentials: true });
    // return this.http.post(this.url + url, param, { headers: httpHeaders, withCredentials: true});
  }


  httpDelete(url: string, options?: any) {
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': '*/*'
    });

    if (options) {
      options.withCredentials = true;
      return this.http.delete(this.url + url, options);
    } else {
      return this.http.delete(this.url + url, {
        headers: httpHeaders, withCredentials: true
        // responseType: 'json'
      });
    }

  }
}
