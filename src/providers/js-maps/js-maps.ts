import { Injectable } from '@angular/core';

declare var google;

@Injectable()
export class JsMapsProvider {

  map: any;
  drawingManager: any;
  coords: any;
  selectedShape: any;

  constructor() {
console.log('dasdsds')
  }

  init(location, element){
    let latLng = new google.maps.LatLng(location.latitude, location.longitude);

    let opts = {
      center: latLng,
      zoom: 11,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    this.map = new google.maps.Map(element.nativeElement, opts);

    var polyOptions = {
      strokeWeight: 0,
      fillOpacity: 0.45,
      editable: true,
      draggable: true
    };
    this.drawingManager = new google.maps.drawing.DrawingManager({
      drawingMode: google.maps.drawing.OverlayType.POLYGON,
      drawingControlOptions: {
        drawingModes: [
          google.maps.drawing.OverlayType.POLYGON
        ]
      },
      markerOptions: {
        draggable: true
      },
      polylineOptions: {
        editable: true
      },
      rectangleOptions: polyOptions,
      circleOptions: polyOptions,
      polygonOptions: polyOptions
    });
    this.drawingManager.setMap(this.map);
    this.drawingManager.setOptions({
      drawingControl: false
    });

    this.drawingManager.setOptions({
      drawingMode:google.maps.drawing.OverlayType.POLYGON
    });


    google.maps.event.addListener(this.drawingManager, 'overlaycomplete', (event) => {
      if (event.type == google.maps.drawing.OverlayType.CIRCLE) {
        var radius = event.overlay.getRadius();
        console.log(radius)
      }
    });
    google.maps.event.addListener(this.drawingManager, 'overlaycomplete', (e) => {
      if (e.type != google.maps.drawing.OverlayType.MARKER) {
        // Switch back to non-drawing mode after drawing a shape.
        this.drawingManager.setDrawingMode(null);
        // To hide:
        this.drawingManager.setOptions({
          drawingControl: false,
          draggable: true
        });

        // Add an event listener that selects the newly-drawn shape when the user
        // mouses down on it.
        var newShape = e.overlay;
        newShape.type = e.type;
        google.maps.event.addListener(newShape, 'click', () => {
          this.setSelection(newShape);
        });
        this.setSelection(newShape);
      }
    });

    // Clear the current selection when the drawing mode is changed, or when the
    // map is clicked.
    google.maps.event.addListener(this.drawingManager, 'drawingmode_changed', ()=>{this.clearSelection});
    google.maps.event.addListener(this.map, 'click', () => {this.clearSelection});
  }



  getCoordsFromWKT() {

  }

  clearSelection() {
    if (this.selectedShape) {
      this.selectedShape.setEditable(false);
      this.selectedShape = null;
    }
  }

  deleteSelection() {
    if (this.selectedShape) {
      this.selectedShape.setMap(null);
      // this.selectedShape = null;
    }
  }

  // getCoordinatesFromShape() {
  //   return this.coords;
  // }

  calculateCoords(obj) {
    this.coords = [];
    for (let i=0; i < obj.getLength(); i++) {
      const xy = obj.getAt(i);
      this.coords.push([xy.lat(), xy.lng()]);
    }
    console.log(this.coords);
  }


  setSelection(shape) {
    const v = shape.getPath();
    this.calculateCoords(v);
    google.maps.event.addListener(v, 'insert_at', (index) => {
      //polygon object: yourPolygon
      this.calculateCoords(shape.getPath());
    });
    google.maps.event.addListener(v, 'set_at', (index) => {
      //polygon object: yourPolygon
      this.calculateCoords(shape.getPath());
    });
    this.clearSelection();
    this.selectedShape = shape;
    shape.setEditable(true);
    shape.setDraggable(true);
  }

}
