import { Injectable } from '@angular/core';
import {Api} from "..";

/*
  Generated class for the GeofenceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GeofenceProvider {

  constructor(public api: Api) {
    console.log('Hello GeofenceProvider Provider');
  }

  getGeofenceDetails(id) {
    return this.api.httpGet('geofences').map((res: Array<any>) => {
      return res.find(i => i.id === id);
    });
  }

  getGeofences() {
    return this.api.httpGet('geofences');
  }

  saveGeofence(geofenceData) {
    if (geofenceData['id']) {
      return this.api.httpPutJson('geofences/' + geofenceData['id'], JSON.stringify(geofenceData));

    } else {
      return this.api.httpPostJson('geofences', JSON.stringify(geofenceData));
    }
  }


}
