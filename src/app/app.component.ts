import {Component, ViewChild} from '@angular/core';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {TranslateService} from '@ngx-translate/core';
import {Config, Nav, NavController, Platform} from 'ionic-angular';
import {Authservice} from "../providers/authservice/authservice";
import {FirstRunPage} from '../pages';
import {Settings} from '../providers';
import {Storage} from "@ionic/storage";
import {UserActivationPage} from "../pages/user-activation/user-activation";
import {Environment} from "@ionic-native/google-maps";
import {HomePage} from "../pages/home/home";
import {LocalNotifications} from "@ionic-native/local-notifications";
import {BackgroundMode} from "@ionic-native/background-mode";
import { Events } from 'ionic-angular';

@Component({
  template: `
    <ion-menu [content]="content">
      <ion-header>
        <ion-toolbar>
          <ion-title>Pages</ion-title>
        </ion-toolbar>
      </ion-header>

      <ion-content>
        <ion-list>
          <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">
            {{p.title}}
          </button>
          <button menuClose ion-item (click)="logoutUser()">
            Logout
          </button>
        </ion-list>
      </ion-content>

    </ion-menu>
    <ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>
  `
})
export class MyApp {
  rootPage = FirstRunPage;
  socket: any;

  @ViewChild(Nav) nav: Nav;

  pages: any[] = [
    {title: 'Tutorial', component: 'TutorialPage'}
  ];

  constructor(private storage: Storage, private translate: TranslateService,
              private platform: Platform, settings: Settings, private config: Config,
              private localNotifications: LocalNotifications,
              public backgroundMode: BackgroundMode,
              public events: Events,
              private statusBar: StatusBar, private splashScreen: SplashScreen,
              private authservice: Authservice) {
    platform.ready().then(() => {
      this.authservice.checkUserSession().subscribe((data) => {
        console.log('User is logged in ')
        console.log(data);
      }, (err) => {
        this.events.publish('user:logout');
      })

      events.subscribe('user:logout', () => {
        this.softLogout();
      });

      // ask for permission for notifications
      localNotifications.hasPermission().then((data) => {
        console.log('has perission of notifications');
      }, (erro) => {
        console.log(erro);
        console.log('no permissions');
        localNotifications.requestPermission().then((data) => {
          console.log('permissions granted');
          console.log(data);
        }, (err) => {
          console.log(err)
          console.log('permissions denied');
        });
      });

      // show notification in background mode
      this.backgroundMode.on('activate').subscribe(() => {
        console.log('bg mode activated');
        // this.showNotification();
      });

      this.backgroundMode.enable();


      Environment.setEnv({
        // api key for server
        'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyDu9Pe8EG443ZJGB_FZ6Qwl0dOzTTnw8ds',
        // api key for local development
        'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyDu9Pe8EG443ZJGB_FZ6Qwl0dOzTTnw8ds'
      });

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.authservice.authenticationState.subscribe(state => {
        this.storage.get('IS_FRESH_INSTALL').then((val) => {
          if (val) {
            if (state) {
              console.log('o m het');
              this.nav.setRoot(HomePage).catch(err => console.log(err));
              this.initializeSocket();
            } else {
              this.nav.push(UserActivationPage).catch(err => console.log(err));
              ;
            }
          } else {
            this.storage.set('IS_FRESH_INSTALL', true)
            // this.router.navigate(['welcome']);
          }
        });
      });
    });
    this.initTranslate();
  }

  initializeSocket() {
    this.socket = new WebSocket('ws://169.53.186.146:8082/' + 'api/socket');
    const that = this;

    this.socket.onclose = function () {
    };

    this.socket.onmessage = function (event) {
      console.log(event.data);
      let data: object = {};
      try {
        data = JSON.parse(event.data);
      } catch (ee) {
      }
      if (data['events']) {
        that.updateEvents(data['events']);
      }
    };
  }

  updateEvents(events) {
    console.log(events);
    // let that = this;
    // Schedule a single notification
    let eventData = [];
    for (let event of events) {
      let text: string;
      if (event['type'] == 'geofenceExit') {
        text = 'Device has exited the Geofence';
      } else if (event['type'] == 'geofenceEnter') {
        text = 'Device has entered the Geofence';
      }
      eventData.push({
        title: 'Geofence Alert',
        id: Math.floor(Math.random() * 1000000),
        text: text,
        // at: date,
        icon: "file://assets/img/icon.png",
        // foreground: true,
        sound: this.platform.is('android') ? 'file://assets/sounds/sound.mp3' : 'file://assets/sounds/beep.caf',
        data: {secret: ""}
      });
    }
    this.localNotifications.schedule(eventData);
  }


  logoutUser() {
    this.authservice.logout().subscribe((data) => {
      console.log(data);
      this.nav.setRoot(UserActivationPage);
      // this.nav.push(UserActivationPage).catch(err => console.log(err));;
    }, (err) => {
      console.log(err);
      this.nav.setRoot(UserActivationPage);
    })
  }

  softLogout() {
   this.authservice.deleteAllCookies().then((data) => {
     this.nav.setRoot(UserActivationPage);
   }, (err) => {
     this.nav.setRoot(UserActivationPage);
   })
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');
    const browserLang = this.translate.getBrowserLang();

    if (browserLang) {
      if (browserLang === 'zh') {
        const browserCultureLang = this.translate.getBrowserCultureLang();

        if (browserCultureLang.match(/-CN|CHS|Hans/i)) {
          this.translate.use('zh-cmn-Hans');
        } else if (browserCultureLang.match(/-TW|CHT|Hant/i)) {
          this.translate.use('zh-cmn-Hant');
        }
      } else {
        this.translate.use(this.translate.getBrowserLang());
      }
    } else {
      this.translate.use('en'); // Set your language here
    }

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
