import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Camera } from '@ionic-native/camera';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule, Storage } from '@ionic/storage';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { Items } from '../mocks/providers/items';
import { Settings, User, Api } from '../providers';
import { MyApp } from './app.component';
import { Authservice } from '../providers/authservice/authservice';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MaterialModule } from "./material.module";
import {QRScanner} from "@ionic-native/qr-scanner";
import {HomePageModule} from "../pages/home/home.module";
import {UserActivationPageModule} from "../pages/user-activation/user-activation.module";
import {UserLocationPageModule} from "../pages/user-location/user-location.module";
import {UserGeofencePageModule} from "../pages/user-geofence/user-geofence.module";
import {UserSettingsPageModule} from "../pages/user-settings/user-settings.module";
import {ProfilePageModule} from "../pages/profile/profile.module";

import { DeviceProvider } from '../providers/device/device';
import {CallNumber} from "@ionic-native/call-number";
import { GeofenceProvider } from '../providers/geofence/geofence';
import {GeofencePageModule} from "../pages/geofence/geofence.module";
import { JsMapsProvider } from '../providers/js-maps/js-maps';
import { LocalNotifications } from '@ionic-native/local-notifications';
import {BackgroundMode} from "@ionic-native/background-mode";

// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function provideSettings(storage: Storage) {
  /**
   * The Settings provider takes a set of default settings for your app.
   *
   * You can add new settings options at any time. Once the settings are saved,
   * these values will not overwrite the saved values (this can be done manually if desired).
   */
  return new Settings(storage, {
    option1: true,
    option2: 'Ionitron J. Framework',
    option3: '3',
    option4: 'Hello'
  });
}

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    BrowserAnimationsModule,
    MaterialModule,
    UserActivationPageModule,
    HomePageModule,
    UserLocationPageModule,
    UserGeofencePageModule,
    GeofencePageModule,
    UserSettingsPageModule,
    ProfilePageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    Api,
    Items,
    User,
    Camera,
    SplashScreen,
    QRScanner,
    StatusBar,
    CallNumber,
    { provide: Settings, useFactory: provideSettings, deps: [Storage] },
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Authservice,
    DeviceProvider,
    GeofenceProvider,
    JsMapsProvider,
    LocalNotifications,
    BackgroundMode
  ]
})
export class AppModule { }
