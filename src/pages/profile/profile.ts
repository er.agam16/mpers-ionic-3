import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DeviceProvider} from "../../providers/device/device";
import {Authservice} from "../../providers/authservice/authservice";
import { Api } from "../../providers/api/api";
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  devices: any;
  updateDevice: any;
  editedDeviceName : String;
  editedPhone1: any;
  editedPhone2: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,  private auth: Authservice, private device: DeviceProvider, private api: Api, private alertCtrl: AlertController, public storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
    this.getAllDevices();

    this.getPhoneNumber1();
    this.getPhoneNumber2();
    
  }
  getPhoneNumber1(){
    this.storage.get('phoneNumber1').then((val) => {
      debugger
      console.log(val)
     this.editedPhone1 = val;
    });
  }
  getPhoneNumber2(){

    this.storage.get('phoneNumber2').then((val) => {
      debugger
      console.log(val)
      this.editedPhone2 = val;
      
    });
  }
  presentAlert(para1, para2) {
    let alert = this.alertCtrl.create({
      title: "Successfully",
      subTitle: para1 + ": " + para2,
      buttons: ['Ok']
    });
    alert.present();
  }

  getAllDevices() {
    this.device.getDevices().subscribe((devices: Array<any>) => {
      this.devices = devices;

      for(let deviceData of devices) {      
        this.updateDevice =   deviceData;  
        this.editedDeviceName = deviceData.name;
        debugger
        console.log(deviceData )
        // console.log(deviceData.id,deviceData.name + " -------------------")
      }
      // this.getAllUserNotifications();
    }, (err) => {
      console.log(err);
    })
  }

  updateName(){
    this.updateDevice.name = this.editedDeviceName;
    debugger
    // console.log(this.editedDeviceName);
    
    // console.log(this.updateDevice);

    if (this.updateDevice['id']) {
      // return this.api.httpPutJson('devices/' + this.updateDevice['id'], JSON.stringify(this.updateDevice));
      let changeName =  this.api.httpPutJson('devices/' + this.updateDevice['id'], this.updateDevice).subscribe((value) => {
        debugger
        console.log( value);
        this.presentAlert("Updated Device Name", this.editedDeviceName);
      }, (err) => {
        alert("Kindly check your internet connection.")
      });
      console.log(changeName)
    } else {
      let changeName =   this.api.httpPutJson('devices/', JSON.stringify(this.updateDevice));
      
    }
  }


  updatePhone1(){
    this.storage.remove('phoneNumber1');
    // console.log(this.updateDevice )
    // debugger
    // console.log(this.editedPhone1)
    let phone_one_req = this.device.sendCustomCommand("123456A1," + this.editedPhone1, this.updateDevice['id']).subscribe((value) => {
      debugger
      console.log(value);

      this.presentAlert("Updated Phone 1", this.editedPhone1);
      this.storage.set('phoneNumber1', this.editedPhone1);
    }, (err) => {
      alert("Kindly check your internet connection.")
    });
    debugger
    console.log(phone_one_req);
  }
  updatePhone2(){
    this.storage.remove('phoneNumber2');
    // console.log(this.updateDevice )
    // debugger
    // console.log(this.editedPhone2)
    let phone_two_req = this.device.sendCustomCommand("123456B1," + this.editedPhone2, this.updateDevice['id']).subscribe((value) => {
      debugger
      console.log(value);
      this.presentAlert("Updated Phone 2", this.editedPhone2);
      this.storage.set('phoneNumber2', this.editedPhone2);
    }, (err) => {
      alert("Kindly check your internet connection.")
    });
    debugger
    console.log(phone_two_req);
  }
  
                    
  
}
