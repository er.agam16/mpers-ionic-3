import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import {Authservice} from "../../providers/authservice/authservice";
import {HomePage} from "../home/home";
import { forkJoin } from "rxjs/observable/forkJoin";
import {DeviceProvider} from "../../providers/device/device";

// import * as Terraformer from "terraformer";
// import * as WKT from "terraformer-wkt-parser";
// var WKT = require('terraformer-wkt-parser');


// import { AuthenticationService } from '../services/authentication.service';
// import { HomePage } from '../home/home';
/**
 * Generated class for the UserActivationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-activation',
  templateUrl: 'user-activation.html',
})
export class UserActivationPage {
  errorString: string;
  activationCode: number;
  isScannerOn: boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private qrScanner: QRScanner,
              private authService: Authservice,
              public loadingCtrl: LoadingController,
              public toastCtrl: ToastController,
              private alertCtrl: AlertController,
              private device: DeviceProvider

  ) {
    this.isScannerOn = false;

    // var geojson = WKT.parse('LINESTRING (30 10, 10 30, 40 40)');

    // console.log(geojson['coordinates'])
  }


  createLoadingIcon(content) {
    let loading = this.loadingCtrl.create({
      content: content
    });

    loading.present();
    return loading;
  }

  activate() {
    if (!this.activationCode) {
      let toast = this.toastCtrl.create({
        message: 'Please enter the Activation Code',
        duration: 3000,
        position: 'top'
      });
      toast.present();
      return false;
    }

    const loginLoader = this.createLoadingIcon(' Please wait while we log you in')
    this.authService.verifyUserCode(this.activationCode).subscribe((data) => {
      console.log(data);
      if(data['status']) {
        this.authService.login(this.activationCode).subscribe((result) => {
          console.log(result);
          loginLoader.dismiss();
          this.navCtrl.setRoot(HomePage);
        }, (err) => {
          loginLoader.dismiss();
          if(err.status == 401) {
            console.log(err);
            // call register API for new users
            let alert = this.alertCtrl.create({
              title: 'Register',
              inputs: [{ name: 'name',placeholder: 'Name'}, { name: 'phone_one',placeholder: 'Phone 1'}, {name: 'phone_two',placeholder: 'Phone 2'}, {name: 'devicePhone',placeholder: 'Device Phone'}],
              buttons: [{
                text: 'Cancel', role: 'cancel',
                handler: data => {
                  console.log('Cancel clicked');
                }
              },{
                text: 'Register',
                handler: (data) => {
                  this.processRegisterationUser(data);
                }
              }
              ]
            });
            alert.present();
          } else {
            console.log(err);
          }
        });
      } else {
        loginLoader.dismiss();
        let toast = this.toastCtrl.create({
          message: 'Seems like you are not a valid user',
          duration: 3000,
          position: 'top'
        });
        toast.present();
      }
    }, (err) => {
      console.log(err);
     loginLoader.dismiss();
   });
    // console.log(isValidUser);
    if(true) {
      return false;
    }
  }

  processRegisterationUser(userPopupData) {
    if(userPopupData.phone_one && userPopupData.phone_two && userPopupData.name && userPopupData.devicePhone) {
      // create the user
      const registerLoader = this.createLoadingIcon(' Please wait while we setup your new account.');
      this.authService.loginAdmin().subscribe((data) => {

        this.authService.register(this.activationCode, userPopupData).subscribe((userRegisterData) => {

            console.log(userRegisterData);
            // TODO change hardcoded value
            // const userData = {
            //   'userId': userRegisterData['id'],
            //   'groupId': 3
            // };
            // attach user to notifications group geofence
            // this.authService.attachPermissions(userData).subscribe((data) => {
              registerLoader.dismiss();
              const deviceRegisterLoader = this.createLoadingIcon(' Please wait while setup the device for you.');

              // logout the admin & login as user
              this.authService.logout().subscribe(() => {
                this.authService.login(this.activationCode).subscribe(() => {

                  // after login add the device
                  // add device to user
                  this.authService.addDeviceToUser(userPopupData['name'], this.activationCode, userPopupData.devicePhone).subscribe((deviceData) => {
                    // Add Notifications for the device
                    let notificationData = {
                      always: true,
                      calendarId: 0,
                      id: -1,
                      notificators: "web",
                      type: "geofenceEnter"
                    };
                    let geofence_entry = this.device.createNotification(notificationData);
                    notificationData.type = 'geofenceExit';
                    let geofence_exit = this.device.createNotification(notificationData);
                    let phone_one_req = this.device.sendCustomCommand("123456A1," + userPopupData.phone_one, deviceData['id']);
                    let phone_two_req = this.device.sendCustomCommand("123456A1," + userPopupData.phone_two, deviceData['id']);
                    forkJoin([geofence_entry, geofence_exit, phone_one_req, phone_two_req]).subscribe(results => {
                      deviceRegisterLoader.dismiss();
                      this.navCtrl.setRoot(HomePage);
                    }, (err) => {
                      console.log(err);
                      deviceRegisterLoader.dismiss();
                    });
                  }, (err) => {
                    console.log(err)
                    deviceRegisterLoader.dismiss();
                  });
                }, (err) => {
                  console.log(err)
                })

              }, (err) => {
                console.log(err)
              });



              // add device to user

            // }, (err) => {
            //   console.log(err)
            //   registerLoader.dismiss();
            // });

          // this.navCtrl.setRoot(HomePage);
        }, (err) => {
          registerLoader.dismiss();
          console.log(err);
          let toast = this.toastCtrl.create({
            message: 'Error while setting up the account',
            duration: 3000,
            position: 'top'
          });
          toast.present();
        })
      }, (err) => {

        registerLoader.dismiss();
        let toast = this.toastCtrl.create({
          message: 'Error while setting up the account',
          duration: 3000,
          position: 'top'
        });
        toast.present();
      });
    } else {
      let toast = this.toastCtrl.create({
        message: 'Please enter all the details',
        duration: 3000,
        position: 'top'
      });
      toast.present();
      return false;
    }
  }


  exitScanner() {
    this.qrScanner.destroy().then((data) => {
      console.log(data);
      window.document.querySelector('ion-app').classList.remove('transparent-app');

    }, (err) => {
      console.log(err);
    });
  }

  /**
   * open scanner
   */
  openScanner() {
    window.document.querySelector('ion-app').classList.add('transparent-app');
    this.isScannerOn = true;

    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          // camera permission was granted
          console.log('Camera permission was granted');

          // start scanning

          const scanSub = this.qrScanner.scan().subscribe((text: string) => {
            console.log('Scanned something', text);

            if(Number.isInteger(Number(text))) {
              this.activationCode = Number(text);
            } else {
              this.errorString = 'Invalid QR code.'
            }
            this.isScannerOn = false;

            // alert(text);
            this.qrScanner.hide(); // hide camera preview
            window.document.querySelector('ion-app').classList.remove('transparent-app');

            scanSub.unsubscribe(); // stop scanning
          });
          this.qrScanner.show();
          console.log(scanSub);

          // this.qrScanner.resumePreview();

          // show camera preview
          // this.qrScanner.show()
          //   .then((data : QRScannerStatus)=> {
          //     // alert(data.showing);
          //     // this.isScannerOn = false;
          //
          //   },err => {
          //     // this.isScannerOn = false;
          //
          //     alert(err);
          //
          //   });

        } else if (status.denied) {
          console.log('Camera permission was denied');
          this.isScannerOn = false;
          // camera permission was permanently denied
          // you must use QRScanner.openSettings() method to guide the user to the settings page
          // then they can grant the permission from there
        } else {
          console.log('Camera permission was denied');
          this.isScannerOn = false;
          // permission was denied, but not permanently. You can ask for permission again at a later time.
        }
      })
      .catch((e: any) => {
        console.log('Error is', e);
        this.errorString = e;
        this.isScannerOn = false;
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserActivationPage');
  }

}
