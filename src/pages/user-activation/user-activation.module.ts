import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserActivationPage } from './user-activation';
import { MaterialModule } from '../../app/material.module';

@NgModule({
  declarations: [
    UserActivationPage,
  ],
  imports: [
    IonicPageModule.forChild(UserActivationPage),
    MaterialModule
  ],
})
export class UserActivationPageModule {}
