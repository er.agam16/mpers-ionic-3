import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserLocationPage} from "../user-location/user-location";
import {CallNumber} from "@ionic-native/call-number";
import { UserGeofencePage } from "../user-geofence/user-geofence";
import {UserSettingsPage} from "../user-settings/user-settings";
import {DeviceProvider} from "../../providers/device/device";
import{ProfilePage} from "../profile/profile";

import { Authservice } from "../../providers/authservice/authservice";
import { Events } from 'ionic-angular';


import { from } from 'rxjs/observable/from';
/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  devices: any;
  devicePhone: any;
  deviceName: any;

  pageMapping: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,private callNumber: CallNumber,
              public events: Events,
              private authService: Authservice, private device: DeviceProvider) {
    this.pageMapping = {
      'UserGeofencePage' : UserGeofencePage,
      'UserLocationPage' : UserLocationPage,

      'UserSettings' : UserSettingsPage,
      'EditProfile' : ProfilePage

  
    };

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    this.getAllDevices()
  }

  getAllDevices() {
    this.device.getDevices().subscribe((devices: Array<any>) => {
      this.devices = devices;

      for(let deviceData of devices) {      
        this.devicePhone =   deviceData.phone;  
        this.deviceName = deviceData.name;
        debugger
        console.log(deviceData )
        // console.log(deviceData.id,deviceData.name + " -------------------")
      }
      // this.getAllUserNotifications();
    }, (err) => {
      console.log(err);
    })
  }

  callPhone() {
    alert(this.deviceName + ": " + this.devicePhone);
    this.callNumber.callNumber(this.devicePhone, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  redirectToPage(page) {
    console.log(page);
    this.navCtrl.push(this.pageMapping[page]).catch((err) => {console.log(err)});
  }



}
