import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DeviceProvider } from "../../providers/device/device";
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  Marker,
  GoogleMapsAnimation, ILatLng, PolylineOptions, Polygon, CameraPosition
} from '@ionic-native/google-maps';
import * as WKT from "terraformer-wkt-parser";
/**
 * Generated class for the UserLocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-location',
  templateUrl: 'user-location.html'
})
export class UserLocationPage {
  map: GoogleMap;
  loading: any;
  socket: WebSocket;
  devices: any;
  markers: object;
  geofences: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public deviceService: DeviceProvider) {
    this.markers = {};
  }


  ionViewDidLoad() {
    this.loadMap();
    console.log('ionViewDidLoad UserLocationPage');
    this.getDevices();
  }

  showGeofence(geof) {

    const geometryDetails = geof['area'];
    if ((geometryDetails.lastIndexOf('POLYGON', 0) === 0)) {
      const parsedGeometry = WKT.parse(geometryDetails);
      let polygon_points: ILatLng[] = [];
      for (let all_rec of parsedGeometry['coordinates']) {
        for(let record of all_rec) {
          polygon_points.push({
            lat: record[0],
            lng: record[1]
          })
        }
      }
      let options: PolylineOptions = {
        'points': polygon_points,
        'strokeColor' : '#718587',
        'fillColor' : '#F0F6F7',
        'strokeWidth': 10
      };
      this.map.addPolygon(options).then((polygon: Polygon) => {
        console.log(polygon);
      }, (err) => {
        console.log(err);
      });
    }
  }


  getGeofences() {
    for (let device of this.devices) {
      this.deviceService.getDeviceGeofences(device.id).subscribe((geofences: Array<any>) => {
        device['geofences'] = geofences;
        for (let geof of geofences) {
          this.showGeofence(geof);
        }
      }, (err) => {
        console.log(err);
      });
      console.log(device.id)
    }
  }



  ionViewWillLeave() {
    this.socket.onclose = function () {}; // disable onclose handler first
    this.socket.close();
  }

  updatePositions(positions) {

    for (const pos of positions) {
      const deviceDetail = this.devices.find(x => x.id === pos.deviceId);
      console.log(deviceDetail.name);
      // deviceDetail.name = 'asdasdas';
      deviceDetail.image_src = (deviceDetail['status'] === 'online')  ?  './assets/img/icons8-online-64.png' :
        './assets/img/icons8-offline-64.png';
      this.map.animateCamera({
        target: {
          'lat' : pos.latitude,
          'lng' : pos.longitude
        },
        zoom: 17,
        tilt: 60,
        bearing: 140,
        duration: 5000
      }).then(() => {
        // alert("Camera target has been changed");
      });

      if (this.markers[pos.deviceId]) {
        this.markers[pos.deviceId].setPosition({
          'lat' : pos.latitude,
          'lng' : pos.longitude
        });
      this.markers[pos.deviceId].setSnippet(deviceDetail['status']);
      } else {
        const marker: Marker = this.map.addMarkerSync({
          title: deviceDetail['name'],
          snippet: deviceDetail['status'],
          icon: (deviceDetail['status'] === 'online') ?  './assets/img/icons8-online-64.png' :
            './assets/img/icons8-offline-64.png',
          position: {
            'lat' : pos.latitude,
            'lng' : pos.longitude
          },
          animation: GoogleMapsAnimation.DROP
        });
        this.markers[pos.deviceId] = marker;

        // show the infoWindow
        marker.showInfoWindow();

        // If clicked it, display the alert
        marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
          console.log('clicked!');
        });
      }
    }
  }

  initializeSocket() {
    this.socket = new WebSocket('ws://169.53.186.146:8082/' + 'api/socket');
    const that = this;

    this.socket.onclose = function () {
      // .app.showToast(Strings.errorSocket, Strings.errorTitle);

      // Ext.Ajax.request({
      //     url: 'api/devices',
      //     success: function (response) {
      //         self.updateDevices(Ext.decode(response.responseText));
      //     },
      //     failure: function (response) {
      //         if (response.status === 401) {
      //             window.location.reload();
      //         }
      //     }
      // });

      // Ext.Ajax.request({
      //     url: 'api/positions',
      //     headers: {
      //         Accept: 'application/json'
      //     },
      //     success: function (response) {
      //         self.updatePositions(Ext.decode(response.responseText));
      //     }
      // });

      // setTimeout(function () {
      //     self.asyncUpdate(false);
      // }, Traccar.Style.reconnectTimeout);
    };

    this.socket.onmessage = function (event) {
      console.log(event.data);
      let data: object = {};
      try {
        data = JSON.parse(event.data);
      } catch (ee) {}
      // var data = Ext.decode(event.data);

      if (data['devices']) {
        that.updateDevices(data['devices']);
      }
      if (data['positions']) {
        that.updatePositions(data['positions']);
        // first = false;
      }
      // if (data.events) {
      //     self.updateEvents(data.events);
      // }
    };
  }

  updateDevices(devices) {
    this.devices = devices;
    for (const dev of this.devices) {
      if (this.markers[dev.id]) {
        this.markers[dev.id].setSnippet(dev['status']);
        let imgSrc = (dev['status'] === 'online') ?
          './assets/img/icons8-online-64.png' :
          './assets/img/icons8-offline-64.png'
        this.markers[dev.id].setIcon(imgSrc);
      }
    }
  }

  getPositions() {
    this.deviceService.getPositions().subscribe((positions: Array<any>) => {
      this.updatePositions(positions);
    }, (err) => {
      console.log(err);
    });
  }

  getDevices() {
    this.deviceService.getDevices().subscribe((data: Array<any>) => {
      console.log(data);
      this.initializeSocket();
      this.devices = data;
      this.getPositions();
      this.getGeofences();
    }, (err) => {
      console.log(err);
    });
  }


  loadMap() {

    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: 43.0741904,
          lng: -89.3809802
        },
        zoom: 18,
        tilt: 30
      }
    };

    this.map = GoogleMaps.create('map_canvas', mapOptions);
  }
}
