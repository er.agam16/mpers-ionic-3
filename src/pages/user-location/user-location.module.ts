import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserLocationPage } from './user-location';
import {MaterialModule} from "../../app/material.module";
import { MomentModule } from 'ngx-moment';

@NgModule({
  declarations: [
    UserLocationPage,
  ],
  imports: [
    IonicPageModule.forChild(UserLocationPage),
    MaterialModule,
    MomentModule
  ],
})
export class UserLocationPageModule {}
