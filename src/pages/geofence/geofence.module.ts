import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GeofencePage } from './geofence';
import { MaterialModule } from '../../app/material.module';

@NgModule({
  declarations: [
    GeofencePage,
  ],
  imports: [
    IonicPageModule.forChild(GeofencePage),
    MaterialModule,
  ],
})
export class GeofencePageModule {}
