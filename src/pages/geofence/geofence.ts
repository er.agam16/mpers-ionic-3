import {Component, ElementRef, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {GeofenceProvider} from "../../providers/geofence/geofence";
import { JsMapsProvider } from '../../providers/js-maps/js-maps';
import { ToastController } from 'ionic-angular';
import {UserGeofencePage} from "../user-geofence/user-geofence";
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapOptions,
  Circle, ILatLng, PolylineOptions, Polygon, CameraPosition, Polyline
} from '@ionic-native/google-maps';
// import * as Terrafomer from "terraformer";
import * as WKT from "terraformer-wkt-parser";
import {DeviceProvider} from "../../providers/device/device";
import {forkJoin} from "rxjs/observable/forkJoin";
import {HomePage} from "../home/home";
import {Authservice} from "../../providers/authservice/authservice";

@IonicPage()
@Component({
  selector: 'page-geofence',
  templateUrl: 'geofence.html',
})
export class GeofencePage {
  map: GoogleMap;

  geofenceId: number;
  geofenceData: any;
  headerTitle: string;
  isMapPreview: boolean;
  isViewMode: boolean;

  @ViewChild('map') mapElement: ElementRef;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private toastCtrl: ToastController,
              private device: DeviceProvider,
              private auth: Authservice,
              private geofence: GeofenceProvider, private mapJS: JsMapsProvider) {
    this.geofenceId = this.navParams.get('geofence_id');
    this.geofenceData = {};
    this.isMapPreview = false;
    this.isViewMode = !!this.navParams.get('isViewMode');


    if  (this.geofenceId) {
      this.headerTitle = 'Edit Geofence';
      if (this.isViewMode) {
        this.headerTitle = 'View Geofence';
      }
      this.getGeofenceDetails(this.geofenceId);
    } else {
      this.headerTitle = 'Add Geofence';
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GeofencePage');
  }

  resetMapType() {
    // this.mapJS
  }

  saveGeoFence() {
    if (this.mapJS.coords) {
      console.log(this.mapJS);
      console.log(this.geofenceData);
      var polygon = WKT.convert(
        {
          "type": "Polygon",
          "coordinates": [this.mapJS.coords]
        }
      );
      console.log(polygon)

      const geofenceData = {
        'name' : this.geofenceData.name,
        'description' : this.geofenceData.description,
        'area' : polygon,
        'calendarId': 0
      };
      if (this.geofenceId) {
        geofenceData['id'] = this.geofenceId;
      }
      this.geofence.saveGeofence(geofenceData).subscribe((geofenceData) => {
        //attach geofence to device
        this.device.getDevices().subscribe((devices: Array<any>) => {

          let device_promises  = [];
          for(let dev of devices) {
            device_promises.push(this.auth.attachPermissions({"deviceId":dev.id,"geofenceId":geofenceData['id']}));
            console.log(dev);
          }

          //attach all devices to geofence
          forkJoin(device_promises).subscribe(results => {
            let toast = this.toastCtrl.create({
              message: 'Genfence has been created',
              duration: 3000,
              position: 'top'
            });
            toast.present();
            this.navCtrl.setRoot(HomePage);
            this.navCtrl.push(HomePage);
          }, (err) => {
            console.log(err);
          });

        }, (err) => {
          console.log(err);
        });
      }, (err) => {
        console.log(err);
      })
    } else {
      let toast = this.toastCtrl.create({
        message: 'Please draw the shape',
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }
  }

  deleteSelection() {
    this.mapJS.deleteSelection();
  }


  closeMapPreviewAddSave() {
    if (this.mapJS.coords) {
      let toast = this.toastCtrl.create({
        message: 'Shape has been added',
        duration: 3000,
        position: 'top'
      });
      toast.present();
      this.isMapPreview = false;
    } else {
      let toast = this.toastCtrl.create({
        message: 'Please draw the Shape',
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }
  }

  closeMapPreviewAddCancel() {
    this.isMapPreview = false;
  }

  openMapPreviewAdd() {
    this.isMapPreview = true;
    console.log(this.mapElement);
    this.mapJS = new JsMapsProvider();
    let location = {
      latitude: '44.3591179',
      longitude: '-79.7357619'
    };
    this.mapJS.init(location, this.mapElement);
    // this.preFillMapShape();
  }


  preFillMapShape() {
    if (this.geofenceData.id) {
      let mapOptions: GoogleMapOptions = {
        camera: {
          target: {
            lat: 43.0741904,
            lng: -89.3809802
          },
          zoom: 18,
          tilt: 30
        }
      };
      this.map = GoogleMaps.create('map_canvas_view', mapOptions);
      const geometryDetails = this.geofenceData['area'];
      let mapArray = [];
      if ((geometryDetails.lastIndexOf('POLYGON', 0) === 0)) {
        const parsedGeometry = WKT.parse(geometryDetails);
        let polygon_points: ILatLng[] = [];
        for (let all_rec of parsedGeometry['coordinates']) {
          for(let record of all_rec) {
            mapArray.push([record[0], record[1]]);
            polygon_points.push({
              lat: record[0],
              lng: record[1]
            })
          }
        }
        this.mapJS.coords = mapArray;
        let options: PolylineOptions = {
          'points': polygon_points,
          'strokeColor' : '#718587',
          'fillColor' : '#F0F6F7',
          'strokeWidth': 10
        };
        this.map.addPolygon(options).then((polygon: Polygon) => {
          console.log(polygon);
          let Camposition: CameraPosition<ILatLng[]> = {
            target: polygon_points,
            zoom: 22,
            tilt: 30
          };
          this.map.moveCamera(Camposition);
        }, (err) => {
          console.log(err);
        });
      }
    }


    /*
    if (this.geofenceData.id) {
      const geometryDetails = this.geofenceData['area'];
      if (geometryDetails) {
        try {
          if((geometryDetails.lastIndexOf('POLYGON', 0) === 0)) {
            const parsedGeometry = WKT.parse(geometryDetails);
            let polygon_points: ILatLng[] = [];
            for (let all_rec of parsedGeometry['coordinates']) {
              for(let record of all_rec) {
                polygon_points.push({
                  lat: record[0],
                  lng: record[1]
                })
              }
            }
            let options: PolylineOptions = {
              'points': polygon_points,
              'strokeColor' : '#AA00FF',
              // 'fillColor' : ,
              'strokeWidth': 10
            };
            this.map.addPolygon(options).then((polygon: Polygon) => {
              console.log(polygon);
              let Camposition: CameraPosition<ILatLng[]> = {
                target: polygon_points,
                zoom: 22,
                tilt: 30
              };
              this.map.moveCamera(Camposition);
            }, (err) => {
              console.log(err);
            });
          }else if ((geometryDetails.lastIndexOf('CIRCLE', 0) === 0)) {

            let geometryDetailsPar = geometryDetails.replace('CIRCLE (', '');
            geometryDetailsPar = geometryDetailsPar.replace(')', '');
            geometryDetailsPar = geometryDetailsPar.split(',');
            let radius = Number(geometryDetailsPar[1]);

            let cords = geometryDetailsPar[0].split(' ');
            let center_cords: ILatLng = {
              lat: Number(cords[0]),
              lng: Number(cords[1])
            };
            // for (let all_rec of parsedGeometry['coordinates']) {
            //   for(let record of all_rec) {
            //     circle_points.push({
            //       lat: record[0],
            //       lng: record[1]
            //     })
            //   }
            // }
            // var center: ILatLng = {
            //   lat: (bbox[1] + bbox[3]) / 2,
            //   lng: (bbox[0] + bbox[2]) / 2
            // };
            //
            // let p = 0.017453292519943295;    // Math.PI / 180
            // let c = Math.cos;
            // let a = 0.5 - c((center.lat-bbox[2]) * p) / 2 + c(bbox[2] * p) *c((center.lat) * p) * (1 - c(((center.lng- bbox[3]) * p))) / 2;
            // let radius = (12742 * Math.asin(Math.sqrt(a))); // 2 * R; R = 6371 km

            // Add circle
            let circle: Circle = this.map.addCircleSync({
              'center': center_cords,
              'radius': radius,
              'strokeColor' : '#AA00FF',
              'strokeWidth': 5,
              'fillColor' : '#880000'
            });
            this.map.moveCamera({
              target: circle.getBounds()
            });
          } else if ((geometryDetails.lastIndexOf('LINESTRING', 0) === 0)) {
            const parsedGeometry = WKT.parse(geometryDetails);
            let polyline_points: ILatLng[] = [];

            for(let record of parsedGeometry['coordinates']) {
              polyline_points.push({
                lat: record[0],
                lng: record[1]
              })
            }
            let poptions: PolylineOptions = {
              'points': polyline_points,
              'strokeColor' : '#AA00FF',
              // 'fillColor' : ,
              'strokeWidth': 10
            };
            this.map.addPolyline(poptions).then((polyline: Polyline) => {
              console.log(polyline);
              let Camposition: CameraPosition<ILatLng[]> = {
                target: polyline_points,
                zoom: 22,
                tilt: 30
              };
              this.map.moveCamera(Camposition);
            }, (err) => {
              console.log(err);
            });
          }
        } catch(ee){
          console.log(ee)
        }
      }
    }
    */
  }






  openMapPreview() {
    this.isMapPreview = true;
    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: 43.0741904,
          lng: -89.3809802
        },
        zoom: 18,
        tilt: 30
      }
    };
    this.map = GoogleMaps.create('map_canvas', mapOptions);
    if (this.geofenceData) {
      const geometryDetails = this.geofenceData['area'];
      if (geometryDetails) {
        try {
          if((geometryDetails.lastIndexOf('POLYGON', 0) === 0)) {
            const parsedGeometry = WKT.parse(geometryDetails);
            let polygon_points: ILatLng[] = [];
            for (let all_rec of parsedGeometry['coordinates']) {
              for(let record of all_rec) {
                polygon_points.push({
                  lat: record[0],
                  lng: record[1]
                })
              }
            }
            let options: PolylineOptions = {
              'points': polygon_points,
              'strokeColor' : '#718587',
              'fillColor' : '#F0F6F7',
              'strokeWidth': 10
            };
            this.map.addPolygon(options).then((polygon: Polygon) => {
              console.log(polygon);
              let Camposition: CameraPosition<ILatLng[]> = {
                target: polygon_points,
                zoom: 22,
                tilt: 30
              };
              this.map.moveCamera(Camposition);
            }, (err) => {
              console.log(err);
            });
          } else if ((geometryDetails.lastIndexOf('CIRCLE', 0) === 0)) {
            let geometryDetailsPar = geometryDetails.replace('CIRCLE (', '');
            geometryDetailsPar = geometryDetailsPar.replace(')', '');
            geometryDetailsPar = geometryDetailsPar.split(',');
            let radius = Number(geometryDetailsPar[1]);

            let cords = geometryDetailsPar[0].split(' ');
            let center_cords: ILatLng = {
              lat: Number(cords[0]),
              lng: Number(cords[1])
            };
            // for (let all_rec of parsedGeometry['coordinates']) {
            //   for(let record of all_rec) {
            //     circle_points.push({
            //       lat: record[0],
            //       lng: record[1]
            //     })
            //   }
            // }
            // var center: ILatLng = {
            //   lat: (bbox[1] + bbox[3]) / 2,
            //   lng: (bbox[0] + bbox[2]) / 2
            // };
            //
            // let p = 0.017453292519943295;    // Math.PI / 180
            // let c = Math.cos;
            // let a = 0.5 - c((center.lat-bbox[2]) * p) / 2 + c(bbox[2] * p) *c((center.lat) * p) * (1 - c(((center.lng- bbox[3]) * p))) / 2;
            // let radius = (12742 * Math.asin(Math.sqrt(a))); // 2 * R; R = 6371 km

            // Add circle
            let circle: Circle = this.map.addCircleSync({
              'center': center_cords,
              'radius': radius,
              'strokeColor' : '#AA00FF',
              'strokeWidth': 5,
              'fillColor' : '#880000'
            });
            this.map.moveCamera({
              target: circle.getBounds()
            });
          } else if ((geometryDetails.lastIndexOf('LINESTRING', 0) === 0)) {
            const parsedGeometry = WKT.parse(geometryDetails);
            let polyline_points: ILatLng[] = [];

            for(let record of parsedGeometry['coordinates']) {
              polyline_points.push({
                lat: record[0],
                lng: record[1]
              })
            }
            let poptions: PolylineOptions = {
              'points': polyline_points,
              'strokeColor' : '#AA00FF',
              // 'fillColor' : ,
              'strokeWidth': 10
            };
            this.map.addPolyline(poptions).then((polyline: Polyline) => {
              console.log(polyline);
              let Camposition: CameraPosition<ILatLng[]> = {
                target: polyline_points,
                zoom: 22,
                tilt: 30
              };
              this.map.moveCamera(Camposition);
            }, (err) => {
              console.log(err);
            });
          }
        } catch(ee){
          console.log(ee)
        }
      }
    }
  }

  getGeofenceDetails(id) {
    this.geofence.getGeofenceDetails(id).subscribe((data) => {
        this.geofenceData = data;
        this.headerTitle = data['name'];
        this.preFillMapShape();
        console.log(data)
    }, (err) => {
      console.log(err);
    })
  }
}
