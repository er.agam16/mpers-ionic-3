import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DeviceProvider} from "../../providers/device/device";
import {Authservice} from "../../providers/authservice/authservice";

/**
 * Generated class for the UserSettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-settings',
  templateUrl: 'user-settings.html',
})
export class UserSettingsPage {

  permissions: any;
  all_permissions: any;
  devices: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private auth: Authservice,
              private device: DeviceProvider) {
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad UserSettingsPage');
    this.getAllDevices();
  }

  getAllDevices() {
    this.device.getDevices().subscribe((devices: Array<any>) => {
      this.devices = devices;
      for(let deviceData of devices) {          
        this.device.getAllNotifications('?deviceId=' + deviceData.id).subscribe((data) => {
          deviceData['device_notifications'] = data;
        }, (err) => {
          console.log(err);
        })
      }
      this.getAllUserNotifications();
    }, (err) => {
      console.log(err);
    })
  }


  getAllUserNotifications() {
    this.device.getAllNotifications().subscribe((data) => {
      this.all_permissions = data;
    }, (err) => {
      console.log(err)
    });
  }

  checkDevicePermission(deviceData, permission_type) {
      if(deviceData['device_notifications']) {
        console.log(deviceData)
        console.log(permission_type);
        return deviceData['device_notifications'].find(i => i.type === permission_type);
      }
      return false;
  }


  togglePermission(deviceData, permission_type, permission_id) {
    let data = {"deviceId":deviceData.id,"notificationId":permission_id};
    if (deviceData['device_notifications'].find(i => i.type === permission_type)) {
      this.auth.removePermissions(data).subscribe((data) => {
        this.getAllDevices();
      }, (err) => {
        console.log(err);
      });
    } else {
      this.auth.attachPermissions(data).subscribe((data) => {
        this.getAllDevices();
      }, (err) => {
        console.log(err);
      });
    }
  }


}
