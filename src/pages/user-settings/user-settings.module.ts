import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserSettingsPage } from './user-settings';
import { MaterialModule} from "../../app/material.module";

@NgModule({
  declarations: [
    UserSettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(UserSettingsPage),
    MaterialModule
  ],
})
export class UserSettingsPageModule {}
