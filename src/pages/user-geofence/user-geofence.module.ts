import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserGeofencePage } from './user-geofence';
import { MaterialModule } from '../../app/material.module';
@NgModule({
  declarations: [
    UserGeofencePage,
  ],
  imports: [
    IonicPageModule.forChild(UserGeofencePage),
    MaterialModule
  ],
})
export class UserGeofencePageModule {}
