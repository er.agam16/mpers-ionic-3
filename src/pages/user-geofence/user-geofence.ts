import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {GeofenceProvider} from "../../providers/geofence/geofence";
import { GeofencePage } from "../geofence/geofence";

/**
 * Generated class for the UserGeofencePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-geofence',
  templateUrl: 'user-geofence.html',
})
export class UserGeofencePage {

  geofenceList: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public geofence: GeofenceProvider) {
    // this.navCtrl.setRoot(UserGeofencePage);
  }


  openGeofencePage(geofenceId = null) {
    this.navCtrl.push(GeofencePage, {
      geofence_id: geofenceId
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad UserGeofencePage');
    this.getGeofences();
  }

  getGeofences() {
    this.geofence.getGeofences().subscribe((data) => {
      this.geofenceList = data;
    }, (err) => {

    });

  }

}
